**Дипломный проект. Приложение.**

Содержит пример приложения на Python с использованием Django и докер файл для сборки образа.<br/>
В настройках CI/CD репозитория необходимо создать следующие переменные:<br/>
CI_ENVIRONMENT (значение devops)<br/>
CI_HELM_CHART_NAME (значение devops-diplom-chart)<br/>
CI_PROJECT_NAME (значение devops-diplom)<br/>
CI_REGISTRY (значение docker.io)<br/>
CI_REGISTRY_IMAGE (значение index.docker.io/alexeyr7/sf-test-app)<br/>
CI_REGISTRY_PASSWORD<br/>
CI_REGISTRY_USER (значение alexeyr7)<br/>
CI_REPO_URL (значение https://lexr7.gitlab.io/DEVOPS_Diplom_infra/)<br/><br/>
Должен использоваться gitlab runner установленный на управляющей ноде.

Последовательность действий для сборки после внесения изменений:<br/>
git add .<br />
git commit -m "commit text here"<br />
git log --oneline<br />
git tag x.x.x yyyy	### где x.x.x - номер версии, а yyyy хэш коммита из вывода команды git log --oneline<br />
git push origin x.x.x<br/>

Далее запускается процесс CI/CD в ходе которого выполняется сборка приложения, его публикация в репозиторий (указанный в настройках) и затем деплой в кластере Kubernetes, используя gitlab runner, развернутый на управляющей ноде.
